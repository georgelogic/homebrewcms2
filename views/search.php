<?php
/**
 * Search.php - renders a search page in the main element of the application
 * 
 * @author Bugslayer
 * 
 */
// Include required external scripts
require_once dirname ( __FILE__ ) . '/../components/search.php';

// Check if the user posted a search command
$isSearch = (isset ( $_GET ['search_text'] ));
$searchCmd = "";
if ($isSearch) {
	$searchCmd = $_GET ['search_text'];
}
?>
<h1>Zoek</h1>
<form method="get">
	<input type="hidden" name="action" value="show" />
	<input type="hidden" name="page" value="search" />
	<div class="form-group">
			<input class="form-control" type="text" name="search_text"
				placeholder="Vul hier uw zoekopdracht in"
				value="<?php if ($isSearch) echo $searchCmd ?>">
	</div>
	<div class="form-group">
		<div class="">
			<button type="submit" name="commit" class="btn btn-default">Zoek</button>
		</div>
	</div>
</form>
<br />
<div id="resultlist">
<?php
// Render the search results only if the user posted a search.
if ($isSearch) {
	$result = search_pattern ( $searchCmd );
	if (sizeof ( $result ) > 0) {
		echo "<ul>";
		foreach ( $result as $row ) {
			echo "<li>";
			echo '<b><a href="?action=show&page=article&id=' . $row ['ID'] . '">' . $row ['Name'] . '</a></b><br/>' . $row ['Content'];
			echo "</li>";
		}
		echo "</ul>";
	} else {
		echo "Geen zoekresultaten gevonden.";
	}
}
?>
</div>
<!-- Include search javascript file -->
<script src="js/search.js"></script>


